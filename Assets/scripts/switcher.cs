using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class switcher : MonoBehaviour
{
    public void switchBeach()
    {
        SceneManager.LoadScene(1);
    }

    public void switchFire()
    {
        SceneManager.LoadScene(2);
    }
    public void switchStarsky()
    {
        SceneManager.LoadScene(3);
    }

    public void Menu()
    {
        SceneManager.LoadScene(0);
    }
}
